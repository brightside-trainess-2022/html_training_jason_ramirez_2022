<h1>March 22, 2022</h1>
<h3>Completed Tasks:</h3>

- *Finished watching [Introduction to Web](https://app.pluralsight.com/library/courses/01dc3ea2-d250-4019-b911-4076b2292838)*
    - Introduction  
    - HTML  
    - Cascading Style Sheets (CSS)  
    - JavaScript  
    - jQuery  
    - AJAX  
    - Command Line  
    - Node.js  
- *Finished watching [HTML and CSS: Creating a Basic Website (Interactive)](https://app.pluralsight.com/library/courses/html-css-basic-website/table-of-contents)*
    - HTML
    - CSS
    - Classes and Layout		
    - Images		
    - Fonts and Forms